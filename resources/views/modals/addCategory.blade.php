<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Pievienot jaunu kategoriju</h4>
            </div>
            <form class="ajax-form form-horizontal" action="{{ route('createCategory') }}" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="category-title" class="col-sm-2 control-label">Nosaukums</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="category-title" name="title" placeholder="Nosaukums">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Atcelt</button>
                    <button type="submit" class="btn btn-primary" data-form="category">Saglablāt</button>
                </div>
            </form>
        </div>
    </div>
</div>