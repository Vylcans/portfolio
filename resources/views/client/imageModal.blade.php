<div class="container">
    <div class="row">
        <div class="image-container col-lg-8 col-lg-offset-2">
            <img src="{{ $image->getSource() }}" class=" img-responsive">
        </div>
        @include('client.imageSocialButtons', ['image' => $image])
    </div>
</div>