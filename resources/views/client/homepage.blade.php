@extends('client.app')
@section('social')
    @include(
        'client.socialTags',
        [
            'url' => route('home'),
            'image' => asset('images/social-main-image.jpg')
        ]
    )
@stop
@section('content')

<div class="page-wrapper">
    <div class="container main-header">
        <div class="row">
            <div class="col-lg-5 col-lg-offset-4">
                <h1 class="main-title homepage-title" id="logo homepage-logo">
                    <img src="{{ asset('images/paraksts-medium.png') }}" class="img-responsive">
                    <span class="hidden">Edgars Vylcāns fotogrāfija</span>
                </h1>
                <h2 class="main-quote">{{ Lang::get('text.intro_text') }}</h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @foreach(\App\Models\Category::orderBy('ordering', 'asc')->get() as $category)
                <div class="col-md-3 main-category-image">
                    <a href="{{ route('openedContent', ['slug' => $category->slugByLocale()])}}" class="main-category-link">
                        <img src="{{ asset($category->getLatestImageSource()) }}" class="img-responsive bordered-wrapper" title="Portrets">
                        <h4>
                            {{ $category->getTitle() }}
                        </h4>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>

@stop