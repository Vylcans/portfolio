<div class="album-wrapper row">
    <h4 class="album-title-simple">{{ $album->titleByLocale() }}</h4>
    @foreach($album->images()->orderBy(\App\Models\Image::ORDER_BY, 'asc')->get() as $image)
        <div class="col-sm-2 text-center">
            <a class="fancybox" rel="group-{{ $album->id }}" href="{{ $image->getUrl() }}">
                <img class="img-thumbnail" src="{{ $image->getSource(true) }}" alt="" />
            </a>
        </div>
    @endforeach
</div>