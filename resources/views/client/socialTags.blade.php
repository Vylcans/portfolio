<meta property="og:url"           content="{{ $url }}" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="{{ Lang::get('text.meta_title') }}" />
<meta property="og:description"   content="{{ Lang::get('text.meta_description') }}" />
<meta property="og:image"         content="{{ $image }}" />
<meta property="fb:app_id" content="1657728127838508" />