@extends('client.app')
@section('social')
    <meta property="og:url"           content="{{ route('openedContent', ['slug' => $image->slugByLocale()]) }}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="{{ Lang::get('text.meta_title') }}" />
    <meta property="og:description"   content="{{ Lang::get('text.meta_description') }}" />
    <meta property="og:image"         content="{{ $image->getSource(true) }}" />
@stop
@section('content')
    <div class="container">
        <div class="row single-image-wrapper">
            @include(
                'client.singleImage',
                [
                    'nextGalleryItemUrl' => $nextGalleryItemUrl,
                    'prevGalleryItemUrl' => $prevGalleryItemUrl,
                    'image' => $image
                ]
            )

        </div>
        @if($album->images()->count() > 1)
        <div class="single-image-album">
            @include('client.album', ['album' => $album])
        </div>
        @endif
    </div>

@stop
@section('css')
    <!-- Add fancyBox -->
    <link rel="stylesheet" href="{{ asset('assets/fancybox/source/jquery.fancybox.css?v=2.1.5') }}" type="text/css" media="screen" />
    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <link rel="stylesheet" href="{{ asset('assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5') }}" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{ asset('assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7') }}" type="text/css" media="screen" />
@stop
@section('js')
    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="{{ asset('assets/fancybox/lib/jquery.mousewheel-3.0.6.pack.js') }}"></script>
    <!-- Add fancyBox -->
    <script type="text/javascript" src="{{ asset('assets/fancybox/source/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <script type="text/javascript" src="{{ asset('assets/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7') }}"></script>
@stop