@if(!is_null($nextGalleryItemUrl))
    <a href="{{ $nextGalleryItemUrl }}" class="image-control keyboard-control next"></a>
@endif
@if(!is_null($prevGalleryItemUrl))
    <a href="{{ $prevGalleryItemUrl }}" class="image-control keyboard-control prev"></a>
@endif
<img src="{{ $image->getSource() }}" class="img-responsive">
@include('client.imageSocialButtons', ['image' => $image])