@extends('client.app')
@section('social')
    @include(
        'client.socialTags',
        [
            'url' => route('home'),
            'image' => asset('images/social-main-image.jpg')
        ]
    )
@stop
@section('content')

<div class="page-wrapper">
    <div class="container main-header">
        <h2 class="text-center">{{ Lang::get('text.title_' . $category->slugByLocale()) }}</h2>
    </div>
    <div class="container">
        @foreach($category->albums()->orderBy('updated_at', 'desc')->get() as $album)
            @if($album->images()->count() > 0)
                @include('client.album', ['album' => $album])
            @endif
        @endforeach
    </div>
</div>

@stop
@section('css')
    <!-- Add fancyBox -->
    <link rel="stylesheet" href="{{ asset('assets/fancybox/source/jquery.fancybox.css?v=2.1.5') }}" type="text/css" media="screen" />
    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <link rel="stylesheet" href="{{ asset('assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5') }}" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{ asset('assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7') }}" type="text/css" media="screen" />
@stop
@section('js')
    <!-- Add mousewheel plugin (this is optional) -->
    <!--
    <script type="text/javascript" src="{{ asset('assets/fancybox/lib/jquery.mousewheel-3.0.6.pack.js') }}"></script>
    -->
    <!-- Add fancyBox -->
    <script type="text/javascript" src="{{ asset('assets/fancybox/source/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <script type="text/javascript" src="{{ asset('assets/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7') }}"></script>
@stop