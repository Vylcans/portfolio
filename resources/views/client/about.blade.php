@extends('client.app')
@section('social')
    @include(
        'client.socialTags',
        [
            'url' => route('home'),
            'image' => asset('images/social-main-image.jpg')
        ]
    )
@stop
@section('content')
    <div class="container">
        <div class="row author-container">
            <h2 class="author-title">{{ $title }}</h2>
            <img src="{{ $image }}" class="img-responsive bordered-wrapper" title="Edgars Vylcāns">
            <div class="about-block">
                <h3 class="author-quote">{{ $quote }}</h3>
                <p class="author-description">
                    {!! $description !!}
                </p>
            </div>
            <div class="about-block">
                <div class="h2">{{ Lang::get('text.social_stuff') }}</div>
                <div class="social-link-block row col-sm-offset-4">
                    <a href="{{ $facebookLink }}" target="_blank" class="social-link facebook-link col-sm-2" title="Facebook">{{ Lang::get('text.facebook_profile') }}</a>
                    <a href="{{ $facebookPageLink }}" target="_blank" class="social-link facebook-page-link col-sm-2" title="Facebook">{{ Lang::get('text.facebook_page') }}</a>
                    <a href="{{ $instagramLink }}" target="_blank"  class="social-link instagram-link col-sm-2" title="Facebook">{{ Lang::get('text.instagram_profile') }}</a>
                </div>
            </div>
            <div class="about-block">
                <div class="h2">{{ Lang::get('text.contact_form') }}</div>
                <div class="contact-form-wrapper row">
                    <form class="contact-form col-md-7 col-lg-offset-2 form-horizontal" method="post" action="{{ route('contactForm') }}">
                        <div class="form-group">
                            <label for="contactName" class="col-sm-2 control-label">{{ Lang::get('text.contact_name') }}</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="contactName" placeholder="{{ Lang::get('text.contact_name') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contactEmail" class="col-sm-2 control-label">{{ Lang::get('text.contact_email') }}</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="email" id="contactEmail" placeholder="{{ Lang::get('text.contact_email') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contactMessage" class="col-sm-2 control-label">{{ Lang::get('text.contact_message') }}</label>
                            <div class="col-sm-10">
                                <textarea class="form-control no-resize" rows="5" name="message" id="contactMessage" placeholder="{{ Lang::get('text.contact_message') }}"></textarea>
                            </div>
                        </div>
                        <input type="text" name="username" class="contact-username">
                        <button type="submit" class="btn btn-default col-md-2 col-md-offset-6 form-submit">
                            {{ Lang::get('text.send') }}
                        </button>
                    </form>
                    <div class="contact-form-thanks hidden">
                        {{ Lang::get('text.thank_you_for_contact_form') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')

@stop