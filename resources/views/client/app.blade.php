<!DOCTYPE html>
<html lang="lv">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Edgars Vylcāns</title>
    <meta name="keywords" content="fotogrāfija, melnbaltā fotogrāfija, portrets, akts, fotogrāfs, photography, black and white, portrait, nude, photographer">
    <meta name="description" content="{{ Lang::get('text.meta_title') }}">

    @section('social') @show

    <link href="{{ asset('/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    @section('css') @show
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <script async defer src="//assets.pinterest.com/js/pinit.js"></script>

    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png?v=1.1">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png?v=1.1">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png?v=1.1">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png?v=1.1">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png?v=1.1">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png?v=1.1">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png?v=1.1">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png?v=1.1">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png?v=1.1">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png?v=1.1">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png?v=1.1">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png?v=1.1">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png?v=1.1">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

</head>
<body class="client @if(is_object(Route::getCurrentRoute()) && Route::getCurrentRoute()->getName() != 'home') body-margin @endif">

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=1657728127838508";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div class="wrap">
    <div id="main">

        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    @if(is_object(Route::getCurrentRoute()) && Route::getCurrentRoute()->getName() != 'home')
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>


                        <a class="navbar-brand" href="{{ route('home') }}">
                            <img alt="Brand" src="{{ asset('images/paraksts-small.png') }}">
                        </a>

                    @endif
                </div>
                <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
                    <ul class="nav navbar-nav navbar-right">
                        @if(is_object(Route::getCurrentRoute()) && Route::getCurrentRoute()->getName() != 'home')
                            @foreach(\App\Models\Category::orderBy('ordering', 'asc')->get() as $category)
                                <li class="@if(isset($activePage) && $activePage == $category->slugByLocale()) active @endif">
                                    <a href="/{{ $category->slugByLocale() }}">
                                        {{ $category->getTitle() }}
                                    </a>
                                </li>
                            @endforeach
                            <li class="@if(isset($activePage) && $activePage == 'about') active @endif"><a href="{{ route('about') }}">{{ Lang::get('text.about_author') }}</a></li>
                        @else
                            <?php
                            $route = '/en';
                            $urlText = 'English';
                            if (\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getCurrentLocale() == 'en') {
                                $route = '/';
                                $urlText = 'Latviešu';
                            }
                            ?>
                            <li class=""><a href="{{ $route }}">{{ $urlText }}</a></li>
                        @endif
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        @yield('content')
    </div>
</div>

<footer class="footer">
    {{ Lang::get('text.copyright_text') }} Edgars Vylcāns © {{ date('Y') }}<br>
    {{ Lang::get('text.dont_click') }} <a href="{{ \App\Models\Language::getDontClickLink() }}" class="dont-click-link" title="{{ Lang::get('text.i_really_want_to_click') }}">{{ Lang::get('text.here') }}</a>

    <div class="dont-click-count-wrapper @if(\App\Models\Click::getCount() == 0) hidden @endif">
        (<span class="dont-click-count">{{ \App\Models\Click::getCount() }}</span> {{ Lang::get('text.times_clicked') }})
    </div>

</footer>

<!-- Scripts -->
<script src="{{ asset('/assets/jquery/jquery-1.11.3.min.js') }}"></script>
<script src="{{ asset('/assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/layout.js') }}"></script>
<script src="{{ asset('/js/app.js') }}"></script>

@section('js') @show

@if(env('APP_ENV') == 'server' and !Auth::check())
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-40087146-1', 'auto');
        ga('send', 'pageview');
    </script>
@endif
</body>
</html>
