@extends('client.app')
@section('content')
    <div class="container text-center">
        <h2 class="not-found-title">
            {{ Lang::get('text.404_title') }}
        </h2>
        <h4 class="not-found-content">
            {{ Lang::get('text.404_content') }}
        </h4>
        <img src="{{ asset('images/404.jpg') }}" class="bordered-wrapper">
    </div>
@stop