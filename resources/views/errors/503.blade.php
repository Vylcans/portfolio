<!DOCTYPE html>
<html lang="lv">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Edgars Vylcāns</title>
    <meta name="keywords" content="fotogrāfija, melnbaltā fotogrāfija, portrets, akts, fotogrāfs, photography, black and white, portrait, nude, photographer">
    <meta name="description" content="{{ Lang::get('text.meta_title') }}">

    <link href="{{ asset('/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png?v=1.1">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png?v=1.1">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png?v=1.1">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png?v=1.1">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png?v=1.1">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png?v=1.1">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png?v=1.1">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png?v=1.1">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png?v=1.1">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png?v=1.1">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png?v=1.1">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png?v=1.1">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png?v=1.1">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

</head>
<body class="client @if(is_object(Route::getCurrentRoute()) && Route::getCurrentRoute()->getName() != 'home') body-margin @endif">
<div class="wrap">
    <div id="main">
jav
        <div class="page-wrapper">
            <div class="container main-header">
                <div class="row" style="margin-top: 100px">
                    <div class="col-lg-5 col-lg-offset-4">
                        <h1 class="main-title homepage-title" id="logo homepage-logo">
                            <img src="{{ asset('images/paraksts-medium.png') }}" class="img-responsive">
                            <span class="hidden">Edgars Vylcāns fotogrāfija</span>
                        </h1>
                        <h2 class="main-quote">{{ Lang::get('text.intro_text') }}</h2>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row text-center">
                    <h2>Šobrīd notiek uzlabošanas darbi.</h2>
                    <h2>Under construction</h2>
                </div>
            </div>
        </div>

    </div>
</div>

<footer class="footer">
    {{ Lang::get('text.copyright_text') }} Edgars Vylcāns © {{ date('Y') }}<br>
    {{ Lang::get('text.dont_click') }} <a href="{{ \App\Models\Language::getDontClickLink() }}" class="dont-click-link" title="{{ Lang::get('text.i_really_want_to_click') }}">{{ Lang::get('text.here') }}</a>

    <div class="dont-click-count-wrapper @if(\App\Models\Click::getCount() == 0) hidden @endif">
        (<span class="dont-click-count">{{ \App\Models\Click::getCount() }}</span> {{ Lang::get('text.times_clicked') }})
    </div>

</footer>

<!-- Scripts -->
<script src="{{ asset('/assets/jquery/jquery-1.11.3.min.js') }}"></script>
<script src="{{ asset('/assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/layout.js') }}"></script>
<script src="{{ asset('/js/app.js') }}"></script>

@section('js') @show
</body>
</html>
