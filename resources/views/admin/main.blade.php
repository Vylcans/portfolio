@extends('admin.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Nepaklausīgie
        </div>
        <div class="panel-body">
            <div class="row">
                <p class="col-sm-1">
                    {{ \App\Models\Click::getCount() }}
                </p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            TOP 10 attēli
        </div>
        <div class="panel-body">
            <div class="row">
                @foreach($images as $image)
                    <div class="col-md-2 admin-image-wrapper">
                        <img src="{{ $image->getSource(true) }}" class="img-responsive bordered-wrapper square-thumbnail">
                        <p>Skatījumi: {{ $image->views }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            Pēdējie 10 skatītie
        </div>
        <div class="panel-body">
            <div class="row">
                @foreach($lastViewed as $image)
                    <div class="col-md-2 admin-image-wrapper">
                        <img src="{{ $image->getSource(true) }}" class="img-responsive bordered-wrapper square-thumbnail">
                        <p>{{ $image->getLastViewedDate() }}</p>
                        <p>{{ $image->referrer }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    @if($topFacebook->count() > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                TOP 10 Facebook attēli
            </div>
            <div class="panel-body">
                <div class="row">
                    @foreach($topFacebook as $image)
                        <div class="col-md-2 admin-image-wrapper">
                            <img src="{{ $image->getSource(true) }}" class="img-responsive bordered-wrapper square-thumbnail">
                            <p>Shares: {{ $image->fb_share }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    @if($topTwitter->count() > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                TOP 10 Twitter attēli
            </div>
            <div class="panel-body">
                <div class="row">
                    @foreach($topTwitter as $image)
                        <div class="col-md-2 admin-image-wrapper">
                            <img src="{{ $image->getSource(true) }}" class="img-responsive bordered-wrapper square-thumbnail">
                            <p>Shares: {{ $image->tw_share }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    @if($topPinterest->count() > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                TOP 10 Pinterest attēli
            </div>
            <div class="panel-body">
                <div class="row">
                    @foreach($topPinterest as $image)
                        <div class="col-md-2 admin-image-wrapper">
                            <img src="{{ $image->getSource(true) }}" class="img-responsive bordered-wrapper square-thumbnail">
                            <p>Shares: {{ $image->pinterest_share }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    <div class="panel panel-default">
        <div class="panel-heading">
            Nepareizie logini
        </div>
        <div class="panel-body">
            <div class="row">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Datums</th>
                            <th>Lietotājvārds</th>
                            <th>Parole</th>
                            <th>IP adrese</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($failedLogins as $login)
                            <tr>
                                <td>
                                    {{ $login->created_at }}
                                </td>
                                <td>
                                    {{ $login->username }}
                                </td>
                                <td>
                                    {{ $login->password }}
                                </td>
                                <td>
                                    {{ $login->ip }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop