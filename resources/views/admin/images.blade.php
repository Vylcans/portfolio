@extends('admin.app')
@section('content')
    <div class="well">
        <div class="row">
            <form class="add-album col-md-4 well no-ajax" method="post" action="{{ route('adminCreateAlbum') }}">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <?php $i = 0; ?>
                        @foreach(\App\Models\Language::getNativeArray() as $id => $native)
                            <li role="presentation" class="@if($i == 0) active @endif">
                                <a href="#language_{{ $id }}" aria-controls="home" role="tab" data-toggle="tab">
                                    {{ $native }}
                                </a>
                            </li>
                            <?php $i++; ?>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        <?php $i = 0; ?>
                        @foreach(\App\Models\Language::getNativeArray() as $id => $native)
                            <div role="tabpanel" class="tab-pane @if($i == 0) active @endif" id="language_{{ $id }}">
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control" name="albumTitle[{{ $id }}]" id="album-title" placeholder="Albuma nosaukums {{ $native }}">
                                </div>
                            </div>
                            <?php $i++; ?>
                        @endforeach
                    </div>
                </div>
                <div class="form-group category-selector">
                    <select class="form-control" name="categoryId" id="category-selector">
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->getTitle() }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-default add-album-button">Pievienot albumu</button>
            </form>
        </div>

        <form class="row image-data add-image">

            <div class="form-group col-md-3 category-selector">
                <select name="albumId" class="form-control">
                    @foreach(\App\Models\Album::getAllAlbumList() as $id => $value)
                        <option value="{{ $id }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>

            <input type="hidden" name="type" value="{{ \App\Models\Image::TYPE_ALBUM_IMAGE }}">
        </form>
        @include('admin.uploadImage')
    </div>


    <ul class="nav nav-tabs" role="tablist">
        <?php $i = 0; ?>
        @foreach($categories as $category)
            <li role="presentation" class="@if($i == 0) active @endif">
                <a href="#category_{{ $category->slugByLocale('lv') }}" aria-controls="home" role="tab" data-toggle="tab">
                    {{ $category->slugByLocale('lv') }}
                </a>
            </li>
            <?php $i++; ?>
        @endforeach
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <?php $i = 0; ?>
        @foreach($categories as $category)
            <div role="tabpanel" class="tab-pane @if($i == 0) active @endif" id="category_{{ $category->slugByLocale('lv') }}">
                @foreach($category->albums()->orderBy('id', 'desc')->get() as $album)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{ $album->titleByLocale() }} <a href="{{ route('adminDeleteAlbum', ['id' => $album->id]) }}">Dzēst</a>
                        </div>
                        <div class="panel-body">
                            <div class="row sortable" id="sortable_{{ $album->id }}">
                                @foreach($album->images()->getOrderedAsc() as $image)
                                    <div class="col-md-2 admin-image-wrapper sortable-item" data-ordering="{{ $image->ordering }}" data-id="{{ $image->id }}">
                                        <a href="#">
                                            <img src="{{ $image->getSource(true) }}" class="img-responsive bordered-wrapper square-thumbnail">
                                        </a>
                                        <p>Skatījumi: {{ $image->views }}</p>
                                        @if(!is_null($image->fb_share))
                                            <p>Facebook: {{ $image->fb_share }}</p>
                                        @endif
                                        @if(!is_null($image->tw_share))
                                            <p>Twitter: {{ $image->tw_share }}</p>
                                        @endif
                                        @if(!is_null($image->pinterest_share))
                                            <p>Pinterest: {{ $image->pinterest_share }}</p>
                                        @endif
                                        <div class="form-group">
                                            <select name="album" class="image-change-album form-control" data-image="{{ $image->id }}">
                                                <option>Mainīt albumu</option>
                                                @foreach(\App\Models\Album::getAllAlbumList() as $id => $value)
                                                    <option value="{{ $id }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <a href="{{ route('adminDeleteImage', ['id' => $image->id]) }}">Dzēst</a>

                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <?php $i++; ?>
        @endforeach

    </div>
@stop
@include('admin.uploadAssets')
@section('additional_js')
    <script>
        $(function() {

            $('.sortable').each(function() {
                $(this).sortable({
                    containment: "parent",
                    dropOnEmpty: true,
                    update: function (event, ui) {

                        var result = [];
                        $(this).find('.sortable-item').each(function () {
                            result.push($(this).data('id'));
                        });

                        $.post('change-ordering', {result: result}, function (response) {
                            console.log(response);
                        })
                    }
                });
            });

            var imageAlbumChangeUrl = '{{ route('adminChangeAlbum') }}';

            $('.image-change-album').each(function () {
                var imageId = $(this).data('image');
                $(this).change(function () {
                    var albumId = $(this).val();
                    $.post(imageAlbumChangeUrl, {imageId: imageId, albumId: albumId}, function (response) {
                        location.reload();
                    })
                })


            });

        });
    </script>
@stop