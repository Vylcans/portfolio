@extends('admin.app')
@section('content')
    <div class="well row">
        <form class="image-data about-text-form add-image col-md-4" method="post" action="{{ route('adminChangeAbout') }}">
            <div>
                <ul class="nav nav-tabs" role="tablist">
                    <?php $i = 0; ?>
                    @foreach(\App\Models\Language::getNativeArray() as $id => $native)
                        <li role="presentation" class="@if($i == 0) active @endif">
                            <a href="#language_{{ $id }}" aria-controls="home" role="tab" data-toggle="tab">
                                {{ $native }}
                            </a>
                        </li>
                        <?php $i++; ?>
                    @endforeach
                </ul>
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach(\App\Models\Language::getNativeArray() as $id => $native)
                        <div role="tabpanel" class="tab-pane @if($i == 0) active @endif" id="language_{{ $id }}">

                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" name="text[{{ $id }}][{{ \App\Models\Text::KEY_ABOUT_TITLE }}]" value="{{ $texts[$id][\App\Models\Text::KEY_ABOUT_TITLE] }}" placeholder="Virsraksts">
                            </div>
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" name="text[{{ $id }}][{{ \App\Models\Text::KEY_ABOUT_QUOTE }}]" value="{{ $texts[$id][\App\Models\Text::KEY_ABOUT_QUOTE] }}" placeholder="Citāts">
                            </div>
                            <div class="form-group col-md-12">
                                <textarea type="text" rows="10" class="form-control no-resize" name="text[{{ $id }}][{{ \App\Models\Text::KEY_ABOUT_DESCRIPTION }}]" placeholder="Apraksts">{{ $texts[$id][\App\Models\Text::KEY_ABOUT_DESCRIPTION] }}</textarea>
                            </div>

                        </div>
                        <?php $i++; ?>
                    @endforeach
                </div>
                <div>
                    <div class="form-group col-md-12">
                        <input type="text" class="form-control" name="public_email" value="{{ $userData['public_email'] }}" placeholder="Epasts">
                    </div>
                    <div class="form-group col-md-12">
                        <input type="text" class="form-control" name="public_phone" value="{{ $userData['public_phone'] }}" placeholder="Tālrunis">
                    </div>
                    <div class="form-group col-md-12">
                        <input type="text" class="form-control" name="facebook_link" value="{{ $userData['facebook_link'] }}" placeholder="Facebook Adrese">
                    </div>
                    <div class="form-group col-md-12">
                        <input type="text" class="form-control" name="facebook_page_link" value="{{ $userData['facebook_page_link'] }}" placeholder="Facebook Lapas Adrese">
                    </div>
                    <div class="form-group col-md-12">
                        <input type="text" class="form-control" name="instagram_link" value="{{ $userData['instagram_link'] }}" placeholder="Instagram Adrese">
                    </div>
                    <button class="btn btn-default" type="submit">Saglabāt</button>
                </div>
            </div>
        </form>
        <div class="col-md-8">
            @include('admin.uploadImage')
        </div>
        <div class="col-md-8">
            <form class="row image-data">
                <input type="hidden" name="type" value="{{ \App\Models\Image::TYPE_AUTHOR_IMAGE }}">
                <div class="row">
                    <img src="{{ Auth::user()->getImageSource() }}" class="img-responsive col-md-12">
                </div>
            </form>
        </div>

    </div>
@stop
@include('admin.uploadAssets')