@section('js')
    <script src="{{ asset('assets/jQuery-File-Upload-9.11.2/js/vendor/jquery.ui.widget.js') }}"></script>

    <script src="{{ asset('assets/jQuery-File-Upload-9.11.2/js/tmpl.min.js') }}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="{{ asset('assets/jQuery-File-Upload-9.11.2/js/load-image.all.min.js') }}"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="{{ asset('assets/jQuery-File-Upload-9.11.2/js/canvas-to-blob.min.js') }}"></script>

    <!-- blueimp Gallery script -->
    <script src="{{ asset('assets/jQuery-File-Upload-9.11.2/js/jquery.blueimp-gallery.min.js') }}"></script>

    <script src="{{ asset('assets/jQuery-File-Upload-9.11.2/js/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('assets/jQuery-File-Upload-9.11.2/js/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('assets/jquery-ui/jquery-ui.js') }}"></script>

    <script src="{{ asset('assets/jQuery-File-Upload-9.11.2/js/jquery.fileupload-process.js') }}"></script>

    <script src="{{ asset('assets/jQuery-File-Upload-9.11.2/js/jquery.fileupload-image.js') }}"></script>

    <script src="{{ asset('assets/jQuery-File-Upload-9.11.2/js/jquery.fileupload-ui.js') }}"></script>

    <script src="{{ asset('js/imageUpload.js') }}"></script>

@stop
@section('css')
    <link href="{{ asset('/assets/jQuery-File-Upload-9.11.2/css/jquery.fileupload.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/jQuery-File-Upload-9.11.2/css/style.css') }}" rel="stylesheet">
@stop