@extends('admin.app')
@section('content')
    @if($categories->count() > 0)
        <table class="table">
            <thead>
                <tr>
                    <th>
                        Nosaukums
                    </th>
                    <th>
                        Albumu skaits
                    </th>
                    <th>
                        Attēlu skaits
                    </th>
                    <th>
                        Skatījumi
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>
                            {{ $category->getTitle() }}
                        </td>
                        <td>
                            {{ $category->albumCount() }}
                        </td>
                        <td>
                            {{ $category->imageCount() }}
                        </td>
                        <td>
                            {{ $category->views }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
@stop