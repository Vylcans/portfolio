<!DOCTYPE html>
<html lang="lv">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Edgars Vylcāns</title>

    <link href="{{ asset('/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">

    @section('css') @show

    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png?v=1.1">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png?v=1.1">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png?v=1.1">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png?v=1.1">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png?v=1.1">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png?v=1.1">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png?v=1.1">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png?v=1.1">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png?v=1.1">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png?v=1.1">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png?v=1.1">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png?v=1.1">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png?v=1.1">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body class="admin @if(is_object(Route::getCurrentRoute()) && Route::getCurrentRoute()->getName() != 'home') body-margin @endif">
<div class="container">

    @if(Auth::check())
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ route('adminMain') }}">
                    <img alt="Brand" src="{{ asset('images/paraksts-small.png') }}">
                </a>
            </div>

            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ route('adminImages') }}">
                        Attēli
                    </a>
                </li>
                <li>
                    <a href="{{ route('adminCategories') }}">
                        Kategorijas
                    </a>
                </li>
                <li>
                    <a href="{{ route('adminGetAbout') }}">
                        Par mani
                    </a>
                </li>
            </ul>
            <a href="/auth/logout" class="navbar-text navbar-right logout-link">
                Iziet
            </a>
        </nav>
    @endif

    @yield('content')
</div>

<footer class="footer">
    {{ Lang::get('text.copyright_text') }} Edgars Vylcāns © {{ date('Y') }}<br>
</footer>

<!-- Scripts -->
<script src="{{ asset('/assets/jquery/jquery-1.11.3.min.js') }}"></script>
<script src="{{ asset('/assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/layout.js') }}"></script>
<script src="{{ asset('/js/app.js') }}"></script>

@section('js') @show
@section('additional_js') @show
</body>
</html>
