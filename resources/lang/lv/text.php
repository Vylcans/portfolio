<?php

return [
    'title_portrets' => 'Portrets',
    'title_akts' => 'Akts',
    'title_ainava' => 'Ainava',
    'title_dazadi' => 'Dažādi',
    'times_clicked' => 'nepaklausīja',
    'dont_click' => 'Nespied',
    'here' => 'šeit',
    'i_really_want_to_click' => 'Es ļoti, ļoti vēlos nospiest',

    'about_author' => 'Par autoru',
    'social_stuff' => 'Sociālie tīkli',
    'contact_form' => 'Sazinies ar mani',
    'facebook_profile' => 'Facebook profils',
    'facebook_page' => 'Facebook lapa',
    'instagram_profile' => 'Instagram profils',
    'contact_name' => 'Vārds',
    'contact_email' => 'E-pasts',
    'contact_message' => 'Ziņojums',
    'send' => 'Sūtīt',
    'message_is_required_field' => 'Lūdzu ieraksti vismaz kaut ko ziņojumā',
    'email_format_wrong' => 'Epasts īsti neizskatās pēc epasta',

    'copyright_text' => 'Visu fotogrāfiju autors ',
    'intro_text' => 'Cilvēku iepazīšana un fotografēšana',

    'meta_title' => 'Edgars Vylcāns: Cilvēku iepazīšana un fotografēšana',
    'meta_description' => 'Edgara Vylcāna portfolio',
    'share_on_facebook' => 'Dalīties Facebook',
    '404_title' => 'Hmm... izskatās, ka šādu saturu vēl neesmu nofotografējis',
    '404_content' => 'Bet iespējams, ka šeit bija jābūt Tavam foto. Vari ar mani sazināties "Par autoru" sadaļā',

    'untitled_album' => 'Untitled',

    'wrong_login' => 'Nepareizs minējums... Mēģini vēl tu, nekrietnais, nekrietnais cilvēks...',
    'login_data_saved' => 'Starpcitu, tavs mēģinājums un IP adrese ir piefiksēti un nodoti autoram, lai pasmejas par variācijām',
];
