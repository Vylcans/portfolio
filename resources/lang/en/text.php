<?php

return [

    'title_portrait' => 'Portrait',
    'title_nude' => 'Nude',
    'title_landscape' => 'Landscape',
    'title_various' => 'Various',

    'times_clicked' => 'didn\'t listen',
    'dont_click' => 'Don\'t click',
    'here' => 'here',
    'i_really_want_to_click' => 'I really, really want to click',

    'about_author' => 'About author',
    'social_stuff' => 'Social links',
    'contact_form' => 'Contact me',
    'facebook_profile' => 'Facebook profile',
    'facebook_page' => 'Facebook page',
    'instagram_profile' => 'Instagram profile',
    'contact_name' => 'Name',
    'contact_email' => 'Email',
    'contact_message' => 'Message',
    'send' => 'Send',
    'message_is_required_field' => 'Please enter at least something in the message',
    'email_format_wrong' => 'Please enter correct email',
    'copyright_text' => 'All images by ',
    'intro_text' => 'Meeting people and taking pictures',
    'meta_title' => 'Edgars Vylcāns: meeting people and taking pictures',
    'meta_description' => 'Portfolio of Edgars Vylcāns',
    'share_on_facebook' => 'Share on Facebook',

    '404_title' => 'Well... looks like I haven\'t photographed this yet.',
    '404_content' => 'But perhaps here should have been Your image.',

    'untitled_album' => 'Untitled',

    'wrong_login' => 'Wrong guess. Try harder You, evil, evil person...',
    'login_data_saved' => 'By the way, your attempt and the IP address has been saved for further fun',
];
