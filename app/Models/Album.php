<?php

namespace App\Models;

class Album extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'albums';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'published',
        'ordering',
        'category_id',
        'views'
    ];

    /*
     * Belongs To
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /*
     * Has Many
     */
    public function images()
    {
        return $this->hasMany('App\Models\Image');
    }

    public function titles()
    {
        return $this->hasMany('App\Models\AlbumTitle');
    }

    /**
     * Gets album title by locale
     * @param null $locale
     * @return string
     */
    public function titleByLocale($locale = null)
    {
        $language = Language::getLanguageByLocale($locale);
        $title = $this->titles()->where('language_id', '=', $language->id)->first();

        $untitled = \Lang::get('text.untitled_album');

        if (!$title) {
            return $untitled;
        }

        $titleContent = $title->content;

        if (empty($titleContent)) {
            return $untitled;
        }

        return $titleContent;
    }

    public static function getAllAlbumList()
    {
        $albums = self::orderBy('category_id', 'asc')->get();

        $result = [];

        /**
         * @var Album $album
         */
        foreach ($albums as $album) {
            /**
             * @var Category $category
             */
            $category = $album->category;
            $result[$album->id] = $album->titleByLocale('lv') . ' (' . $category->getTitle() . ')';
        }

        return $result;
    }
}
