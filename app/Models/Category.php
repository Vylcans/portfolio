<?php

namespace App\Models;

class Category extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'published',
        'ordering',
        'views'
    ];

    /*
     * Has Many
     */
    public function albums()
    {
        return $this->hasMany('App\Models\Album');
    }

    public function slugs()
    {
        return $this->hasMany('App\Models\Slug');
    }

    public function images()
    {
        return $this->hasManyThrough('App\Models\Image', 'App\Models\Album');
    }

    /**
     * @return integer
     */
    public function albumCount()
    {
        return $this->albums()->count();
    }

    public function imageCount()
    {
        return $this->images()->count();
    }

    public function slugListForImage()
    {
        $result = [];

        foreach ($this->slugs as $slug) {
            $result[$slug->language_id] = $slug->name;
        }

        return $result;
    }

    public function getLatestImage()
    {
        return $this->images()->orderBy('id', 'desc')->first();
    }

    public function getLatestImageSource()
    {
        $image = $this->getLatestImage();

        if ($image) {
            return asset(Image::UPLOAD_PATH . '/' . Image::THUMB_PREFIX . $image->filename);
        }

        return asset('images/no-image.jpg');
    }

    public function getTitle()
    {
        return \Lang::get('text.title_' . $this->slugByLocale());
    }
}
