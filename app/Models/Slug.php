<?php

namespace App\Models;

class Slug extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'slugs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'language_id',
        'category_id',
        'image_id',
    ];

    /*
     * Belongs To
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function image()
    {
        return $this->belongsTo('App\Models\Image');
    }

    public function language()
    {
        return $this->belongsTo('App\Models\Language');
    }

    public function hasCategory()
    {
        return !is_null($this->category);
    }

    public function hasImage()
    {
        return !is_null($this->image);
    }
}
