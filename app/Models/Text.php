<?php

namespace App\Models;

class Text extends BaseModel
{
    const KEY_ABOUT_TITLE = 'about_title';
    const KEY_ABOUT_QUOTE = 'about_quote';
    const KEY_ABOUT_DESCRIPTION = 'about_description';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'texts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'value',
        'language_id',
    ];

    public $timestamps = false;

    /*
     * Belongs To
     */
    public function language()
    {
        return $this->belongsTo('App\Models\Language');
    }

    public function scopeLanguageId($query, $languageId)
    {
        return $query->where('language_id', '=', $languageId);
    }

    public function scopeKey($query, $key)
    {
        return $query->where('key', '=', $key);
    }

    /**
     * @param $key
     * @param null $locale
     * @return null|string
     */
    public static function getValue($key, $locale = null)
    {
        $language = Language::getLanguageByLocale($locale);

        $text = self::languageId($language->id)->key($key)->first();

        if ($text) {
            return $text->value;
        }

        return null;
    }

    public static function getAboutKeys()
    {
        return [
            self::KEY_ABOUT_TITLE,
            self::KEY_ABOUT_QUOTE,
            self::KEY_ABOUT_DESCRIPTION
        ];
    }

    /**
     * @param null $languageId
     * @return mixed
     */
    public static function getAboutTexts($languageId = null)
    {
        $aboutKeys = self::getAboutKeys();

        $query = self::whereIn('key', $aboutKeys);

        if (!is_null($languageId)) {
            $query->languageId($languageId);
        }

        return $query->get();
    }

    /**
     * Return text by key and language id
     * If does not exist - creates with empty value
     * @param $key
     * @param $languageId
     * @return static
     */
    public static function getTextByKeyAndLanguageId($key, $languageId)
    {
        $text = self::languageId($languageId)->key($key)->first();

        if ($text) {
            return $text;
        }

        return self::create([
            'language_id' => $languageId,
            'key' => $key
        ]);
    }

    public static function getAuthorTitle($locale = null)
    {
        return self::getValue(self::KEY_ABOUT_TITLE, $locale);
    }

    public static function getAuthorQuote($locale = null)
    {
        return self::getValue(self::KEY_ABOUT_QUOTE, $locale);
    }

    public static function getAuthorDescription($locale = null)
    {
        return self::getValue(self::KEY_ABOUT_DESCRIPTION, $locale);
    }


}
