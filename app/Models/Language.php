<?php

namespace App\Models;

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Language extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'languages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'published',
        'native',
        'locale',
    ];

    /*
     * Belongs To
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Slug');
    }

    /*
     * Has Many
     */
    public function images()
    {
        return $this->hasMany('App\Models\Image');
    }

    /**
     * Gets array of languages in pairs of locale and id
     * @return mixed
     */
    public static function getLocaleIdArray()
    {
        return self::lists('id', 'locale');
    }

    /**
     * @param $locale
     * @return Language|null
     */
    public static function getLanguageByLocale($locale = null)
    {
        if (is_null($locale)) {
            $locale = LaravelLocalization::getCurrentLocale();
        }

        return self::where('locale', '=', $locale)->first();
    }

    public static function getNativeArray()
    {
        return self::orderBy('native', 'desc')->lists('native', 'id');
    }

    /**
     * @return string
     */
    public static function getDontClickLink()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $link = route('gribuNospiest');
        if ($locale == 'en') {
            $link = route('wantToClick');
        }

        return $link;
    }
}
