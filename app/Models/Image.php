<?php

namespace App\Models;

use Intervention\Image\Constraint;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Intervention\Image\Facades\Image as InterventionImage;

class Image extends BaseModel
{

    const TYPE_ALBUM_IMAGE = 0;
    const TYPE_AUTHOR_IMAGE = 1;
    const UPLOAD_PATH = '/images/uploads';
    const THUMB_PREFIX = 'thumb_';

    const ORIENTATION_PORTRAIT = 0;
    const ORIENTATION_LANDSCAPE = 1;
    const ORDER_BY = 'ordering';
    const SHARE_FACEBOOK = 0;
    const SHARE_TWITTER = 1;
    const SHARE_PINTEREST = 2;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'filename',
        'published',
        'ordering',
        'album_id',
        'views',
        'last_viewed_at',
        'orientation',
        'fb_share',
        'tw_share',
        'pinterest_share',
        'referrer'
    ];

    /*
     * Belongs To
     */
    public function album()
    {
        return $this->belongsTo('App\Models\Album');
    }

    /*
     * Has Many
     */
    public function slugs()
    {
        return $this->hasMany('App\Models\Slug');
    }

    public function getSource($thumbnail = false)
    {
        $image = $this->filename;
        if ($thumbnail) {
            $image = self::THUMB_PREFIX . $image;
        }

        return asset(self::UPLOAD_PATH . '/' . $image);
    }

    /**
     * @param UploadedFile $file
     * @param string $uploadPath
     * @param int $albumId
     * @param array $slugs
     * @return string|Image
     */
    public static function uploadImage(UploadedFile $file, $uploadPath, $albumId, $slugs = [])
    {
        $albumImage = $file->type == self::TYPE_ALBUM_IMAGE;

        $filename = self::createFilename();

        if (!$albumImage) {
            //profile image
            self::createProfileImage($file, $uploadPath, $filename);
        } else {
            //album image
            self::createAlbumImage($file, $uploadPath, $filename, $albumId, $slugs);
        }

        return $filename;
    }

    /**
     * @param UploadedFile $file
     * @param $uploadPath
     * @param $filename
     */
    public static function createProfileImage(UploadedFile $file, $uploadPath, $filename)
    {
        $image = InterventionImage::make($file->getRealPath());
        $image->resize(900, null, function (Constraint $constraint) {
            $constraint->aspectRatio();
        })
            ->save($uploadPath . '/' . $filename);
    }

    /**
     * @param UploadedFile $file
     * @param $uploadPath
     * @param $filename
     * @param $albumId
     * @param array $slugs
     */
    public static function createAlbumImage(UploadedFile $file, $uploadPath, $filename, $albumId, $slugs = [])
    {
        $canvasMaxWidth = 1000;
        $canvasMaxHeight = 800;
        $sideWidth = 20;
        $bottomHeight = 100;

        $orientation = self::ORIENTATION_LANDSCAPE;

        $landscape = true;

        $image = InterventionImage::make($file->getRealPath());

        $currentImageWidth = $image->width();
        $currentImageHeight = $image->height();

        if ($currentImageWidth < $currentImageHeight) {
            $landscape = false;
        }

        $watermarkY = 10;
        $watermarkX = 0;

        $ratio = $currentImageWidth / $currentImageHeight;
        if ($landscape) {
            //landscape

            $imageWidth = $canvasMaxWidth - $sideWidth * 2;
            $imageHeight = null;

            $canvasHeight = ($canvasMaxWidth / $ratio) + $bottomHeight + $sideWidth;
            $canvasWidth = $canvasMaxWidth;

        } else {
            //portrait

            $imageHeight = $canvasMaxHeight - $sideWidth - $bottomHeight;
            $imageWidth = null;

            $canvasHeight = $canvasMaxHeight;
            $canvasWidth = $canvasMaxHeight * $ratio;

            $orientation = self::ORIENTATION_PORTRAIT;

            $watermarkY = 0;
        }

        //lets create canvas on size 900 x 900
        $canvas = InterventionImage::canvas($canvasWidth, $canvasHeight, '#ffffff');

        $watermark = InterventionImage::make(public_path('images/' . 'watermark.png'))
            ->resize(150, null, function (Constraint $constraint) {
                $constraint->aspectRatio();
            })
            ->opacity(70);

        $image->resize($imageWidth, $imageHeight, function (Constraint $constraint) {
            $constraint->aspectRatio();
        })
            ->sharpen(3)
        ;

        $canvas->insert($image, 'top', $sideWidth, $sideWidth)
            ->insert($watermark, 'bottom-right', $watermarkX, $watermarkY)
            ->save($uploadPath . '/' . $filename);


        //creating thumbnail
        InterventionImage::make($file->getRealPath())->fit(300, 300)
            ->save($uploadPath . '/' . self::THUMB_PREFIX . $filename);

        /**
         * @var Image $image
         */
        $image = self::create([
            'filename' => $filename,
            'published' => false,
            'ordering' => self::getNewOrdering($albumId),
            'album_id' => $albumId,
            'orientation' => $orientation
        ]);

        foreach ($slugs as $languageId => $slug) {
            $image->slugs()->create([
                'name' => $slug . '_' . str_replace('.jpg', '', $filename),
                'language_id' => $languageId
            ]);
        }

    }

    /**
     * @param null $additional
     * @return string
     */
    public static function createFilename($additional = null)
    {
        $filename = (string)(time() . rand(1000, 9999));
        if (!is_null($additional)) {
            $filename .= $additional;
        }

        $filename = $filename . '.jpg';

        $exists = Image::where('filename', '=', $filename)->exists();
        if ($exists) {
            return self::createFilename(rand(1000, 9999));
        }

        return $filename;
    }

    /**
     * Deletes image file
     */
    public function deleteFile()
    {
        $filename = $this->filename;
        $file = public_path((Image::UPLOAD_PATH . '/' . $filename));
        $thumbnail = public_path((Image::THUMB_PREFIX . '/' . Image::UPLOAD_PATH . '/' . $filename));

        if (!is_null($filename) and is_file($file)) {
            unlink($file);
        }

        if (!is_null($thumbnail) and is_file($thumbnail)) {
            unlink($thumbnail);
        }
    }

    /**
     * @return null|string
     */
    public function getNextGalleryItemUrl()
    {
        $nextImageUrl = $this->getNextImageUrl();

        if (!is_null($nextImageUrl)) {
            return $nextImageUrl;
        }

        return $this->getFirstAlbumImageUrl();
    }

    /**
     * @return null|string
     */
    public function getPrevGalleryItemUrl()
    {
        $prevImageUrl = $this->getPrevImageUrl();

        if (!is_null($prevImageUrl)) {
            return $prevImageUrl;
        }

        return $this->getLastAlbumImageUrl();
    }

    /**
     * @return null|string
     */
    public function getNextImageUrl()
    {
        $ordering = self::ORDER_BY;
        $nextImageOrdering = self::where($ordering, '>', $this->$ordering)
            ->where('album_id', '=', $this->album_id)
            ->min('ordering');

        if ($nextImageOrdering) {

            $image = self::where('album_id', '=', $this->album_id)
                ->where('ordering', '=', $nextImageOrdering)
                ->first();

            if ($image) {
                return $image->getUrl();
            }
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getPrevImageUrl()
    {
        $ordering = self::ORDER_BY;
        $prevImageOrdering = self::where($ordering, '<', $this->$ordering)
            ->where('album_id', '=', $this->album_id)
            ->max('ordering');

        if (!is_null($prevImageOrdering)) {
            $image = self::where('album_id', '=', $this->album_id)
                ->where('ordering', '=', $prevImageOrdering)
                ->first();

            if ($image) {
                return $image->getUrl();
            }
        }

        return null;
    }

    public function getFirstAlbumImageUrl()
    {
        $ordering = self::ORDER_BY;
        $image = self::where('album_id', '=', $this->album_id)
            ->where('id', '!=', $this->id)
            ->orderBy($ordering, 'asc')
            ->first();

        if ($image) {
            return $image->getUrl();
        }

        return null;
    }

    public function getLastAlbumImageUrl()
    {
        $ordering = self::ORDER_BY;
        $image = self::where('album_id', '=', $this->album_id)
            ->where('id', '!=', $this->id)
            ->orderBy($ordering, 'desc')
            ->first();

        if ($image) {
            return $image->getUrl();
        }

        return null;
    }

    public function getUrl()
    {
        return route('openedContent', ['slug' => $this->slugByLocale()]);
    }

    public function increaseShare($media)
    {
        $field = 'fb_share';
        if ($media == self::SHARE_TWITTER) {
            $field =  'tw_share';
        }

        if ($media == self::SHARE_PINTEREST) {
            $field = 'pinterest_share';
        }

        $this->update([
            $field => $this->$field + 1
        ]);
    }

    public function getLastViewedDate()
    {
        $env = env('APP_ENV');

        $date = new \DateTime($this->last_viewed_at);

        if ($env == 'server') {
            $date->modify('+2 hours 35 minutes');
        }

        return $date->format('H:i d.m.Y');
    }

    public function updateReferrer($referrer)
    {
        $this->update([
            'referrer' => $referrer
        ]);
    }
}
