<?php

namespace App\Models;

class AlbumTitle extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'album_titles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content',
        'language_id',
        'album_id',
    ];

    /*
     * Belongs To
     */
    public function album()
    {
        return $this->belongsTo('App\Models\Album');
    }

    public function language()
    {
        return $this->belongsTo('App\Models\Language');
    }
}
