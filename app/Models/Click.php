<?php

namespace App\Models;

class Click extends BaseModel
{

    const MAX_FROM_SINGLE_IP = 5;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clicks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ip',
    ];

    public static function getCount()
    {
        return self::all()->count();
    }

    public static function getCountByIp($ip)
    {
        if (empty($ip)) {
            return 0;
        }

        return self::where('ip', '=', $ip)->get()->count();
    }

}
