<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class BaseModel extends Model
{
    /**
     * Gets ordering for new entity
     * @param int $parentId
     * @return int
     */
    public static function getNewOrdering($parentId = null)
    {
        $query = self::orderBy('ordering', 'desc');

        if (!is_null($parentId)) {
            $query->where('album_id', '=', $parentId);
        }

        $lastItem = $query->first();

        if ($lastItem) {
            return $lastItem->ordering + 1;
        }

        return 0;
    }

    public function scopeGetOrderedAsc($query)
    {
        return $query->orderBy('ordering', 'asc')->get();
    }

    /**
     * @param $locale
     * @return mixed
     */
    public function slugByLocale($locale = null)
    {
        if (is_null($locale)) {
            $locale = LaravelLocalization::getCurrentLocale();
        }

        $languageByLocale = Language::getLanguageByLocale($locale);

        $slug = $this->slugs()->where('language_id', '=', $languageByLocale->id)->first();

        if (!$slug) {
            return null;
        }

        return $slug->name;
    }

    public function increaseViews()
    {
        $update = [
            'views' => $this->views + 1
        ];

        if ($this instanceof Image) {
            $update['last_viewed_at'] = new \DateTime();
        }

        $this->update($update);
    }
}
