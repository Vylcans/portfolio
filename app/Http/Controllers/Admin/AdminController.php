<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Album;
use App\Models\Category;
use App\Models\FailedLogin;
use App\Models\Image;
use App\Models\Language;
use App\Models\Text;
use Illuminate\Support\Facades\View;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMain()
    {
        $topImages = Image::orderBy('views', 'desc')->limit(12)->get();
        $lastViewed = Image::orderBy('last_viewed_at', 'desc')->whereNotNull('last_viewed_at')->limit(12)->get();

        $topFbShares = Image::orderBy('fb_share', 'desc')->where('fb_share', '>', 0)->limit(12)->get();
        $topTwShares = Image::orderBy('tw_share', 'desc')->where('tw_share', '>', 0)->limit(12)->get();
        $topPinterestShares = Image::orderBy('pinterest_share', 'desc')
            ->where('pinterest_share', '>', 0)->limit(12)->get();

        $logins = FailedLogin::orderBy('id', 'desc')->get();

        return view(
            'admin.main',
            [
                'images' => $topImages,
                'lastViewed' => $lastViewed,
                'topFacebook' => $topFbShares,
                'topTwitter' => $topTwShares,
                'topPinterest' => $topPinterestShares,
                'failedLogins' => $logins
            ]
        );
    }

    /**
     * Main view for the categories
     * @return View
     */
    public function getCategories()
    {
        return view(
            'admin.categories',
            [
                'categories' => Category::orderBy('ordering', 'asc')->get()
            ]
        );
    }

    /**
     * @return View
     */
    public function getImages()
    {
        return view(
            'admin.images',
            [
                'categories' => Category::orderBy('ordering', 'asc')->get()
            ]
        );
    }

    /**
     * Creates new album
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreateAlbum()
    {
        $categoryId = \Input::get('categoryId');

        $album = Album::create([
            'published' => false,
            'category_id' => $categoryId
        ]);
        $albumTitles = \Input::get('albumTitle');
        foreach ($albumTitles as $languageId => $title) {
            $album->titles()->create([
                'content' => $title,
                'language_id' => $languageId
            ]);
        }

        return redirect()->back();
    }

    /**
     * Deletes album
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDeleteAlbum($id)
    {
        $album = Album::find($id);

        if ($album) {
            $images = $album->images;

            foreach ($images as $image) {
                $image->delete();
            }

            $album->delete();
        }

        return redirect()->back();
    }

    /**
     * @return array
     */
    public function postUploadImage()
    {
        $files = \Input::file('files');
        if (count($files) == 1 and $files[0] == null) {
            /*
             * JS sends first request as
             * array:1 [
             *    0 => null
             *  ]
             */
            return \Response::json([]);
        }

        $type = \Input::get('type');

        $slugList = [];
        $albumId = null;

        if ($type == Image::TYPE_ALBUM_IMAGE) {

            $albumId = \Input::get('albumId');
            $album = Album::find($albumId);

            /**
             * @var Category $category
             */
            $category = $album->category;
            $slugList = $category->slugListForImage();

            $album->touch();

        }

        $assetPath = Image::UPLOAD_PATH;
        $uploadPath = public_path($assetPath);

        $i = 0;

        foreach ($files as $file) {
            $createThumbnail = true;
            if ($type == Image::TYPE_AUTHOR_IMAGE) {
                $createThumbnail = false;
            }

            $file->type = $type;

            $filename = Image::uploadImage($file, $uploadPath, $albumId, $slugList, $createThumbnail);

            $i++;

            if ($type == Image::TYPE_AUTHOR_IMAGE and $i > 0) {
                //for author image take only first image
                $user = \Auth::user();
                $user->deleteImage();
                $user->update([
                    'image' => $filename
                ]);

                break;
            }
        }

        $deleteUrl = null;

        // return our results in a files object
        return \Response::json([
            'files' => [
                [
                    'url' =>  '../images/uploads/' . $filename,
                    'thumbnail_url' =>  '../images/uploads/' . Image::THUMB_PREFIX . '_' . $filename,
                    'name' => $filename,
                    'type' =>  'image/jpeg ',
                    'size' => 46353,
                    'delete_url' =>  $deleteUrl,
                    'delete_type' =>  'DELETE '
                ]
            ],
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function postChangeOrdering()
    {
        $result = \Input::get('result');

        foreach ($result as $ordering => $imageId) {
            Image::where('id', '=', $imageId)->update([
                'ordering' => $ordering
            ]);
        }

        return \Response::json([]);
    }

    /**
     * @return View
     */
    public function getAbout()
    {
        $languages = Language::all();

        $parsed = [];

        $aboutKeys = Text::getAboutKeys();

        foreach ($languages as $language) {
            $keyArray = [];
            foreach ($aboutKeys as $key) {
                $keyArray[$key] = null;
            }

            $parsed[$language->id] = $keyArray;
        }

        $texts = Text::getAboutTexts();

        foreach ($texts as $text) {
            $parsed[$text->language_id][$text->key] = $text->value;
        }

        $user = \Auth::user();

        return view(
            'admin.about',
            [
                'texts' => $parsed,
                'userData' => [
                    'public_email' => $user->public_email,
                    'public_phone' => $user->public_phone,
                    'facebook_link' => $user->facebook_link,
                    'facebook_page_link' => $user->facebook_page_link,
                    'instagram_link' => $user->instagram_link,
                    'image' => $user->image,
                ],
            ]
        );
    }

    /**
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postChangeAbout()
    {
        $texts = \Input::get('text');

        foreach ($texts as $languageId => $text) {
            foreach ($text as $key => $value) {
                $entry = Text::getTextByKeyAndLanguageId($key, $languageId);
                if ($entry->value != $value) {
                    $entry->update([
                        'value' => $value
                    ]);
                }
            }
        }


        $user = \Auth::user();
        $user->update([
            'public_email' => \Input::get('public_email'),
            'public_phone' => \Input::get('public_phone'),
            'facebook_link' => \Input::get('facebook_link'),
            'facebook_page_link' => \Input::get('facebook_page_link'),
            'instagram_link' => \Input::get('instagram_link'),
        ]);

        return \Response::json([]);
    }

    /**
     * Deletes image
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getImageDelete($id)
    {
        $image = Image::find($id);

        $album = $image->album;

        $image->delete();

        $album->load('images');

        if (count($album->images) == 0) {
            $album->delete();
        }

        return \Redirect::back();
    }

    /**
     * Changes album for the image
     * @return \Illuminate\Http\JsonResponse
     */
    public function postChangeAlbum()
    {
        $albumId = \Input::get('albumId');
        $imageId = \Input::get('imageId');

        /**
         * @var Image $image
         */
        $image = Image::find($imageId);

        $originalAlbum = $image->album;

        $album = Album::find($albumId);
        if ($album) {
            $image->update([
                'album_id' => $albumId,
                'ordering' => $image->getNewOrdering($albumId)
            ]);
            $album->touch();
        }

        $originalAlbum->load('images');

        if (count($originalAlbum->images) == 0) {
            $originalAlbum->delete();
        }

        return \Response::json([]);
    }
}
