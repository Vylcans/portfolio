<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Click;
use App\Models\Image;
use App\Models\Slug;
use App\Models\Text;
use App\Models\User;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getHome()
    {
        return view('client.homepage');
    }

    /**
     * View for image or category
     */
    public function getCategoryOrImage($slug)
    {
        $slug = Slug::where('name', '=', $slug)->first();

        if (!$slug) {
            abort(404);
        }

        /**
         * @var Slug $slug
         */
        if ($slug->hasCategory()) {
            $category = $slug->category;
            if (!\Auth::user() and !self::isBot()) {
                $category->increaseViews();
            }

            return view(
                'client.category',
                [
                    'category' => $category,
                    'activePage' => $category->slugByLocale()
                ]
            );
        }

        if ($slug->hasImage()) {
            $view = 'client.image';
            $data = [];
            /**
             * @var Image $image
             */
            $image = $slug->image;

            if (!\Auth::user() and !self::isBot()) {
                $image->increaseViews();
                $image->updateReferrer(\Request::header('referer'));
            }

            if (\Request::ajax()) {
                $view = 'client.imageModal';
            }

            if (!\Request::ajax()) {
                $data['album'] = $image->album;
            }

            $data['image'] = $image;
            $data['activePage'] = $image->album->category->slugByLocale();

            $data['nextGalleryItemUrl'] = $image->getNextGalleryItemUrl();
            $data['prevGalleryItemUrl'] = $image->getPrevGalleryItemUrl();

            return view($view, $data);
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAbout()
    {
        /**
         * @var User $user
         */
        $user = User::first();

        return view(
            'client.about',
            [
                'email' => $user->public_email,
                'phone' => $user->public_phone,
                'image' => $user->getImageSource(),
                'facebookLink' => $user->facebook_link,
                'facebookPageLink' => $user->facebook_page_link,
                'instagramLink' => $user->instagram_link,
                'title' => Text::getAuthorTitle(),
                'quote' => Text::getAuthorQuote(),
                'description' => Text::getAuthorDescription(),
                'activePage' => 'about'
            ]
        );
    }

    /**
     * Increases "don't click"
     * @return \Illuminate\Http\JsonResponse
     */
    public function postClick()
    {
        $ip = \Request::getClientIp();

        $entryCount = Click::getCountByIp($ip);

        if ($entryCount < Click::MAX_FROM_SINGLE_IP) {
            Click::create([
                'ip' => $ip
            ]);
        }

        return \Response::json([
            'count' => Click::getCount()
        ]);
    }

    /**
     * Sends email from contact form
     * @param Requests\ContactFormRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postContactForm(Requests\ContactFormRequest $request)
    {
        $honeyPot = \Input::get('username');
        if (!empty($honeyPot)) {
            return \Response::json([]);
        }

        $name = \Input::get('name');
        $email = \Input::get('email');
        $message = \Input::get('message');

        $user = User::first();

        $to = $user->email;

        $publicEmail = $user->public_email;
        if ($publicEmail) {
            $to = $publicEmail;
        }

        $messageData = [
            'content' => $message,
            'name' => $name,
            'email' => $email
        ];

        \Mail::send('emails.contactMessage', $messageData, function ($m) use ($to) {
            $m->to($to)->subject('Jauns ziņojums no vylcans.lv');
        });

        return \Response::json([]);
    }

    public function postShareImage($id)
    {
        $media = \Input::get('media');

        /**
         * @var Image $image
         */
        $image = Image::find($id);

        if ($image) {
            $image->increaseShare($media);
        }
    }

    private static function isBot()
    {
        if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/bot|crawl|slurp|spider/i', $_SERVER['HTTP_USER_AGENT'])) {
            return true;
        }

        return false;
    }
}
