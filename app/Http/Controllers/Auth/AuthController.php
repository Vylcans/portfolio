<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\FailedLogin;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

class AuthController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    protected $redirectTo = '/admin';

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard  $auth
     * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
     * @return void
     */
    public function __construct(Guard $auth, Registrar $registrar)
    {
        $this->auth = $auth;
        $this->registrar = $registrar;

        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|email', 'password' => 'required',
        ]);

        $credentials = $request->only('username', 'password');

        $credentials['email'] = $credentials['username'];
        unset($credentials['username']);

        if ($this->auth->attempt($credentials, $request->has('remember'))) {
            return redirect()->intended($this->redirectPath());
        }

        $loginData = [
            'username' => \Input::get('username'),
            'password' => \Input::get('password'),
            'ip' => \Input::getClientIp(),
        ];

        FailedLogin::create($loginData);

        return redirect($this->loginPath())
            ->with('loginData', $loginData)
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                //'email' => $this->getFailedLoginMessage(),
                'loginData' => \Lang::get('text.login_data_saved')
            ]);
    }

}
