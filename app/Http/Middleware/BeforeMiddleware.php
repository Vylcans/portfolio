<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\Middleware;

class BeforeMiddleware implements Middleware
{

    public function handle($request, Closure $next)
    {
        $underConstruction = !!env('UNDER_CONSTRUCTION', false);

        if ($underConstruction) {
            $safeIpList = explode(',', env('UNDER_CONSTRUCTION_IP_LIST', ''));
            $clientIp = $request->getClientIp();

            if (!in_array($clientIp, $safeIpList)) {
                \App::abort(503);
            }
        }

        return $next($request);
    }
}