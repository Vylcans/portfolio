<?php

namespace App\Http\Requests;

class ContactFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required',
            'email' => 'email'
        ];
    }

    public function messages()
    {
        return [
            'message.required' => \Lang::get('text.message_is_required_field'),
            'email.email' => \Lang::get('text.email_format_wrong'),
        ];
    }
}
