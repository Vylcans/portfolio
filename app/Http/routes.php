<?php

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::post('/image/{id}/share', [
    'as' => 'shareImage',
    'uses' => 'ClientController@postShareImage'
]);

Route::post('/i-really-want-to-click', [
    'as' => 'wantToClick',
    'uses' => 'ClientController@postClick'
]);

Route::post('/es-loti-gribu-nospiest', [
    'as' => 'gribuNospiest',
    'uses' => 'ClientController@postClick'
]);

Route::post('/contact-form', [
    'as' => 'contactForm',
    'uses' => 'ClientController@postContactForm'
]);

Route::get('/login', [
    'as' => 'loginScreen',
    'uses' => 'AdminController@getLogin'
]);


Route::group([
    'prefix' => 'admin',
    'middleware' => [
        'auth'
    ],
    'namespace' => 'Admin'
], function () {
    Route::get('/', [
        'as' => 'adminMain',
        'uses' => 'AdminController@getMain'
    ]);
    Route::get('/images', [
        'as' => 'adminImages',
        'uses' => 'AdminController@getImages'
    ]);
    Route::get('/categories', [
        'as' => 'adminCategories',
        'uses' => 'AdminController@getCategories'
    ]);
    Route::post('/upload', [
        'as' => 'adminUpload',
        'uses' => 'AdminController@postUploadImage'
    ]);
    Route::post('/change-ordering', [
        'as' => 'adminChangeOrdering',
        'uses' => 'AdminController@postChangeOrdering'
    ]);
    Route::get('/about', [
        'as' => 'adminGetAbout',
        'uses' => 'AdminController@getAbout'
    ]);
    Route::post('/about', [
        'as' => 'adminChangeAbout',
        'uses' => 'AdminController@postChangeAbout'
    ]);

    Route::post('/images/change-album', [
        'as' => 'adminChangeAlbum',
        'uses' => 'AdminController@postChangeAlbum'
    ]);

    Route::get('/images/{id}/delete', [
        'as' => 'adminDeleteImage',
        'uses' => 'AdminController@getImageDelete'
    ]);

    Route::post('/albums/create', [
        'as' => 'adminCreateAlbum',
        'uses' => 'AdminController@postCreateAlbum'
    ]);

    Route::get('/albums/{id}/delete', [
        'as' => 'adminDeleteAlbum',
        'uses' => 'AdminController@getDeleteAlbum'
    ]);

});

Route::group(['prefix' => LaravelLocalization::setLocale()], function () {

    Route::get('/', [
        'as' => 'home',
        'uses' => 'ClientController@getHome'
    ]);

    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
    Route::get('/vylcans', [
        'as' => 'about',
        'uses' => 'ClientController@getAbout'
    ]);
    Route::get('/{slug}', [
        'as' => 'openedContent',
        'uses' => 'ClientController@getCategoryOrImage'
    ]);
});
