<?php

use Illuminate\Database\Seeder;

class BasicDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        self::createLanguages();
        self::createCategories();


    }

    private static function createLanguages()
    {
        $languages = \App\Models\Language::all();
        if ($languages->count() == 0) {
            $languageData = [
                [
                    'locale' => 'lv',
                    'native' => 'Latviešu',
                    'published' => true
                ],
                [
                    'locale' => 'en',
                    'native' => 'English',
                    'published' => true
                ],
            ];

            foreach ($languageData as $lData) {
                \App\Models\Language::create($lData);
            }
        }
    }

    private static function createCategories()
    {
        $localeToId = \App\Models\Language::getLocaleIdArray();

        $categories = \App\Models\Category::all();

        DB::table('categories')->delete();

        $categoryData = [
            [
                'published' => true,
                'ordering' => 0,
                'slugs' => [
                    'lv' => 'portrets',
                    'en' => 'portrait'
                ]
            ],
            [
                'published' => true,
                'ordering' => 1,
                'slugs' => [
                    'lv' => 'akts',
                    'en' => 'nude'
                ]
            ],
            [
                'published' => true,
                'ordering' => 2,
                'slugs' => [
                    'lv' => 'ainava',
                    'en' => 'landscape'
                ]
            ],
            [
                'published' => true,
                'ordering' => 3,
                'slugs' => [
                    'lv' => 'dazadi',
                    'en' => 'various'
                ]
            ],
        ];

        foreach ($categoryData as $data) {
            $category = \App\Models\Category::create([
                'published' => $data['published'],
                'ordering' => $data['ordering']
            ]);

            foreach ($data['slugs'] as $locale => $slug) {
                $category->slugs()->create([
                    'name' => $slug,
                    'language_id' => $localeToId[$locale]
                ]);
            }
        }

    }
}
