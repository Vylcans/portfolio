<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = \App\Models\User::first();

        if (!$admin) {
            \App\Models\User::create([
                'name' => 'Edgars Vylcāns',
                'email' => 'vylcans@gmail.com',
                'password' => Hash::make(env('USER_PASSWORD', 'secret'))
            ]);
        }
    }
}
