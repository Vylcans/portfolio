<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DontClickTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clicks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip')->nullable();

            $table->timestamps();
        });

        $existingClicks = \App\Models\Click::getCount();

        for ($i = 0; $i < $existingClicks; $i++) {
            \App\Models\Click::create([
                'ip' => null
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
