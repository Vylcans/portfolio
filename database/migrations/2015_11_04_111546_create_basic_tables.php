<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('languages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('locale', 2);
            $table->string('native', 20);
            $table->boolean('published')->default(false);

            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('published')->default(false);
            $table->integer('ordering')->unsigned();

            $table->timestamps();
        });

        Schema::create('albums', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('published')->default(false);
            $table->integer('ordering')->unsigned();

            $table->timestamps();

            $table->integer('category_id')->unsigned()->nullable();
        });

        Schema::table('albums', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });

        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filename');
            $table->boolean('published')->default(false);
            $table->integer('ordering')->unsigned();

            $table->timestamps();

            $table->integer('album_id')->unsigned()->nullable();
        });

        Schema::table('images', function (Blueprint $table) {
            $table->foreign('album_id')->references('id')->on('albums')->onDelete('cascade');
        });

        Schema::create('slugs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');

            $table->timestamps();

            $table->integer('category_id')->unsigned()->nullable()->default(null);
            $table->integer('image_id')->unsigned()->nullable()->default(null);
            $table->integer('language_id')->unsigned()->nullable()->default(null);
        });

        Schema::table('slugs', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
