var Form = {
    submit: function ($form) {
        if (typeof $form === 'string') {
            $form = $($form);
        }
        var data = Form.getValues($form);
        var url = $form.attr('action');

        if ($form.hasClass('contact-form')) {
            $('.contact-form').addClass('hidden');
        } {
            $form.addClass('in-progress');
        }

        return Request.post(url, data).always(function () {
            $form.removeClass('in-progress');
        });
    },
    getValues: function ($form) {
        if (typeof $form === 'string') {
            $form = $($form);
        }

        var data = new FormData($form[0]);
        $form.find('input[type="file"]').each(function() {
            var $uploader = $(this);
            var name = $uploader.attr('name');
            $.each($uploader[0].files, function (i, value) {
                data.append(name, value);
            });
        });

        var $editorContent = $form.find('.editor-content-holder');
        if ($editorContent.length) {
            $editorContent.each(function () {
                var editorData = $(this).data('name');
                var editorContent = $(this).html();

                data.append(editorData, editorContent);
            });
        }

        return data;
    },
    showError: function($form, errors) {
        $form.removeClass('hidden');
        if (typeof $form === 'string') {
            $form = $($form);
        }

        $('.contact-error-wrapper').remove();

        var $errorHolder = $('<div />').addClass('contact-error-wrapper').append($('<ul />'));

        for (var k in errors) {
            $errorHolder.find('ul').append($('<li />').addClass('text-danger').text(errors[k]));
        }

        $('.contact-form-wrapper').prepend($errorHolder);

        var $scrollElement = $(window);
        var scrollTop = $errorHolder.offset().top;

        $scrollElement.scrollTop(scrollTop);
    },
    hideError: function($form) {
        if (typeof $form === 'string') {
            $form = $($form);
        }

        $form.find('.alert-danger').remove();
    },
};

var Request = {
    post: function (url, data) {
        var options = {
            type: "POST",
            url: url,
            data: data,
            dataType: "json"
        };

        if (data instanceof FormData) {
            options.contentType = false;
            options.processData = false;
        }

        return $.ajax(options);
    },
    get: function (url, parameters, callBack) {
        if (callBack === undefined) {
            callBack = parameters;
            parameters = {};
        }

        return $.getJSON(url, parameters, callBack);
    }
};