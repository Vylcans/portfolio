$(document).ready(function () {

    console.log('Hello, my dear friend. Here is nothing interesting happening at all. Have a nice day. :) Vylcāns')

    $('body').animate({
        'opacity' : 1
    }, 1000);

    if ($(".fancybox").length > 0) {

        /*
        $(".fancybox").fancybox({
            type: 'ajax'
        });
        */
    }

    if ($('.dont-click-link').length > 0) {
        $('.dont-click-link').on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.post(url, {}, function (response) {
                console.log(response.count);
                $('.dont-click-count').html(response.count);
                if($('.dont-click-count-wrapper ').hasClass('hidden')) {
                    $('.dont-click-count-wrapper ').removeClass('hidden')
                }
            })
        })
    }

    function postToFeed(title, desc, url, image){
        console.log(url);
        var obj = {method: 'feed',link: url, picture: image,name: title,description: desc};
        function callback(response){}
        FB.ui(obj, callback);
    }

    $(document).on("click", ".fbShare", function(e) {
        e.preventDefault();

        var elem = $(this);

        increaseShares(elem);

        postToFeed(elem.data('title'), elem.data('desc'), elem.prop('href'), elem.data('image'));

        return false;
    });


    function TwitterSay( title, url){
        window.open(
            "http://twitter.com/share?text=" + encodeURIComponent( title ) +
            "&url=" + encodeURIComponent( url ),
            "",
            "location=1,status=1,scrollbars=0,resizable=0,width=530,height=400");
        return false;
    }

    $(document).on('click','.twitterSay', function (e) {

        e.preventDefault();

        var $link = $(this);
        increaseShares($link);

        var url = $(this).attr('href');
        var text = $(this).attr('data-text');
        TwitterSay ( text, url );
    });

    $(document).on('click','.pinIt', function (e) {

        e.preventDefault();

        var $link = $(this);
        increaseShares($link);

        window.open($link.attr('href'));

        return false;
    });

    function increaseShares($link)
    {
        var url = $('.imageShareLink').val();
        var media = $link.data('media');

        $.post(url, {media:media}, function (response) {

        });
    }

    $('a').on('click', function (e) {
        if ($(this).attr('href') == '#') {
            e.preventDefault();
        }
    });

    $('form [type="submit"]').on('click', function(ev) {
        var $button = $(this);
        var $form = $button.parents('form');
        if ($form.attr('method').toLowerCase() == 'post' && !$form.hasClass('no-ajax')) {
            ev.preventDefault();

            Form.submit($form).success(function (response) {

                if ($('.contact-form-thanks.hidden').length > 0) {
                    $('.contact-form-thanks.hidden').removeClass('hidden')
                    $('.contact-error-wrapper').remove();
                } else {
                    location.reload();
                }

            }).error(function(response) {

                var errors = response.responseJSON;
                if (errors.message !== undefined) {
                    errors = [errors.message];
                }
                Form.hideError($form);
                Form.showError($form, errors);

            });
        }
    });

    if ($('.keyboard-control').length > 0) {
        $(document).keydown(function(e) {
            switch(e.which) {
                case 37: // left
                    var url = $('.keyboard-control.prev').attr('href');
                    window.location.replace(url);
                    break;

                case 39: // right
                    var url = $('.keyboard-control.next').attr('href');
                    window.location.replace(url);
                    break;

                default: return; // exit this handler for other keys
            }
            e.preventDefault(); // prevent the default action (scroll / move caret)
        });
    }
});